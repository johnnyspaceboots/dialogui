# DialogUI.js #

DialogUI was a fun little project to produce a JS library that would create and manage various kinds of dialog boxes akin to those found in 1994s EarthBound for the Super Nintendo.

Check out an online demo [here](http://104.167.117.58/dialogui/Example/default.htm) (Uses arrow & enter keys)

Check out the project documentation prepared in LaTeX [here](http://104.167.117.58/dialogui/usermanual.pdf)